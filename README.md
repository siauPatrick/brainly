# Brainly

During the screening, you will be asked to solve a programming problem. 
All of the materials that are used in daily work are allowed (including StackOverflow, 
language or library documentation, etc.). While solving the problem you are encouraged to
provide a verbose, verbal, description of the currently tackled bit of the solution.

Once the screening is complete please push to the repository all of the code that is 
created during the meeting.
